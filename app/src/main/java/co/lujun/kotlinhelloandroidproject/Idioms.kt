package co.lujun.kotlinhelloandroidproject

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import java.io.File

/**
 * Created by lujun on 2015/10/15.
 */
class Idioms : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_idioms)

    }

    /**
     * 方法参数的默认值
     */
    fun defauleValueFun(a: Int = 0, b: String = ""){
        // some code
    }

    /**
     * list filter
     */
    fun filterList(list: List<Int>){
        val positive = list.filter { x -> x > 0 }
    }

    /**
     * string interpolation
     */
    fun stringInterpolation(str: String){
        print("string interpolation $str");
    }

    fun instanceChecks(x: Object){
        when(x){
            is Object -> print("Object")
            else -> print("Other")
        }
    }

    /**
     * travesing a map
     */
    fun travesingMap(map: Map<Int, String>){
        for((k, v) in map){
            println("$k -> $v");
        }
        //accessing map
        println(map[1]);
//        map["key"] = "value";
    }

    /**
     * ranges
     */
    fun testRanges(i: Int){
        val list = listOf<Int>(1, 2, 3);// read only list
        val map = mapOf<Int, String>(1 to "1", 2 to "2");// read only map
        for(i in 1..100){
            print("$i");
        }
    }

    /**
     * lazy property
     */
    val p: String by lazy {
        // compute the string
        p
    }

    /**
     * extension functions
     */
    fun String.spaceToCamelCase(){
        print("Convert this to camelcase".spaceToCamelCase());
    }

    /**
     * create a singleton
     */
    object Resource{
        val name = "Name";
    }

    /**
     * if not null then shortland
     */
    fun shorthand(){
        val files = File("test").listFiles();
        println(files?.size());
    }

    /**
     * if not null and else shortland
     */
    fun elseShortland(data: String){
        val files = File("test").listFiles();
        println(files?.size() ?: "empty");

        // executing a statement if null
        val data = data ?: throw IllegalStateException("null");
        // executing if not null
        data?.let {
            // code block
        }
    }

    /**
     * return on when statement
     */
    fun transform(color: String): Int{
        return when(color){
            "Red" -> 1
            "Yellow" -> 2
            else -> throw IllegalArgumentException("illegal argument exception")
        }
    }

    /**
     * try/catch
     */
    fun tryCatch(){
        val result = try{
            // some code

        }catch(e: ArithmeticException){
            throw IllegalStateException(e);
        }
    }

    /**
     * if
     */
    fun ifTest(param: Int){
        val result = if(param == 1){
            "one"
        }else if(param == 2){
            "two"
        }else{
            "three"
        }
    }

    /**
     * Builder-style usage of methods that return Unit
     */
    fun arrayOfMinusOnes(size: Int){
//        return IntArray(size).apply { fill(-1) }
    }


    /**
     * single-expression functions
     */
    fun theAnswer() = 42;
    // equal to
//    fun theAnswer(): Int{
//        return 42;
//    }


    /**
     * combined
     */
    fun transform2(color: String): Int = when(color) {
        "Red" -> 1
        "Yellow" -> 2
        else -> throw IllegalArgumentException("illegal argument exception")
    }
}
