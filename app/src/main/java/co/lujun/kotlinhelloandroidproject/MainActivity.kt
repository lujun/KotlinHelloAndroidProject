package co.lujun.kotlinhelloandroidproject

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Menu
import android.view.MenuItem

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        val fab = findViewById(R.id.fab) as FloatingActionButton
        fab.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                Snackbar.make(view, "a + b = " + sum(1, 2) , Snackbar.LENGTH_LONG).setAction("Action", null).show()
            }
        })
        //
        Log.i(TAG, "sum()-" + sum(1, 2));
        Log.i(TAG, "sum2()-" + sum2(2, 3));
        Log.i(TAG, "sum3()-" + sum3(3, 4));
        printSum(4, 5);

        val btnTestActivity = findViewById(R.id.btn_test_activity);
//        val intent = Intent(this, TestActivity::class.java);
        val intent = Intent(this, Idioms::class.java);
        btnTestActivity.setOnClickListener { startActivity(intent) };
    }

    /**方法**/

    /**
     * Kotlin中定义一个方法
     * @param a Kotlin中的Int类型的参数
     * @param b Kotlin中的Int类型的参数
     * @return 一个Kotlin中的Int类型的值
     */
    fun sum(a: Int, b: Int): Int{
        return a + b;
    }

    /**
     * Kotlin中定义一个方法，使用表达式的方法体和推断类型返回值
     * @param a Kotlin中的Int类型的参数
     * @param b Kotlin中的Int类型的参数
     * @return 一个Kotlin中的Int类型的值
     */
    fun sum2(a: Int, b: Int) = a + b;

    /**
     * Kotlin中定义一个方法，一个方法如果是public的，就应该有明确的返回值
     * @param a Kotlin中的Int类型的参数
     * @param b Kotlin中的Int类型的参数
     * @return 一个Kotlin中的Int类型的值
     */
    public fun sum3(a: Int, b: Int): Int = a + b;

    /**
     * Kotlin中定义一个方法，Unit可以被省略，写成public fun printSum(a: Int, b: Int){}
     * @param a Kotlin中的Int类型的参数
     * @param b Kotlin中的Int类型的参数
     * @return 返回无意义的值（空）
     */
    public fun printSum(a: Int, b: Int): Unit{
        Log.d(TAG, "printSum()-" + (a + b));
    };

    /**变量**/
    val TAG = "MainActivity";

    fun defineVariable(){
        //局部变量定义
            //不可变变量
            val p1: Int = 1;
            // or 省略类型
            val p2 = 1;
            // or 当没有初始值的时候需要类型
            val p3: Int;
            p3=1;// 再初始化

            //可变变量
            var p4 = 5;// 省略Int类型
            p4 += 1;
    }

    /**使用String模板**/
    fun useStringTemplate(args : Array<String>){
        if(args.size() == 0) return;
        print("First argument: ${args[0]}")
    }

    /**使用条件表达式**/
    fun useConditionExpression(a: Int, b: Int): Int{
        if(a > b){
            return a;
        }else{
            return b;
        }
    }
    // 使用if作表达式
    fun useIfAsExpression(a: Int, b: Int) = if (a > b) a else b;

    /**
     * 使用可空类型（nullable）变量来判断null，使用?标记一个变量可空，例如：
     * ------------------------
     * var a: String = "abc";
     * a = null;// error
     * ------------------------
     * var b: String? = "abc";
     * b = null;// ok
     * ------------------------
     * 当一个引用可能是null的时候，它必须显示的被标记为nullable
     */
    fun parseInt(str: String): Int?{
        return null;
    }

    /**
     * 类型检测和自动转换
     * is操作符会检查一个表达式是否是该类型，如果检测到一个局部变量或者属性是这个类型，就会自动对其进行转换
     */

    fun checkType(obj: Any): Int?{
        if(obj is String){
            // `obj` is automatically cast to `String` in this branch
            return obj.length();
        }
        // `obj` is still of type `Any` outside of the type-checked branch
        return null;

        //-------------------or
//        if (obj !is String) {
//            return null;
//        }

        // `obj` is automatically cast to `String` in this branch
//        return obj.length;

        //--------------------or
//        if(obj is String && obj.length() > 0){
//            return obj.length();
//        }
//        return null;
    }

    /**
     * for循环,in操作符
     */
    fun testFor(args: Array<String>){
        for(arg in args){
            print(arg);
        }
        //---------------------or
//        for(i in args.indices){
//            print(args[i]);
//        }
    }

    /**
     * while循环
     */
    fun testWhile(args: Array<String>){
        var i = 0;
        while(i < args.size()){
            print(args[i++]);
        }
    }

    /**
     * if表达式可以是代码段，最后一行的表达式作为段的返回值
     */
    fun testIf(a: Int, b: Int){
        val max = if (a > b) {
            print("Choose a");
            a;
        }
        else {
            print("Choose b");
            b;
        }
        //当if仅仅有一个分支, 或者其中一个分支的返回结果Unit, 它的类型Unit.
    }

    /**
     * when表达式
     * when 将它的参数和所有的分支条件进行比较，直到某个分支满足条件。
     * when既可以被当做表达式使用也可以被当做语句使用。
     * 如果它被当做表达式, 符合条件的分支的值就是整个表达式的值，
     * 如果当做语句使用，则忽略单个分支的值。(就像if,每一个分支可以是一个代码块,它的值是最后的表达式的值.)
     * else 分支将被执行如果其他分支都不满足条件。
     * 如果 when 作为一个表达式被使用,else 分支是必须的，除非编译器能够检测出所有的可能情况都已经覆盖了。
     */
    fun testWhen(obj: Any){
        when(obj){
            1 -> print("x == 1")
            2 -> print("x == 2")
            else -> { // Note the block
                print("x is neither 1 nor 2")
            }
        }

//        如果很多分支需要用相同的方式处理，则可以把多个分支条件放在一起, 用,逗号分隔:

//        when (x) {
//            0, 1 -> print("x == 0 or x == 1")
//            else -> print("otherwise")
//        }

        //可以在判断分支条件的地方使用任何表达式，而不仅仅是常量(和switch不同)

//        也可以检查一个值 in 或者 !in 一个 范围 或者集合:

        /*when (x) {
            in 1..10 -> print("x is in the range")
            in validNumbers -> print("x is valid")
            !in 10..20 -> print("x is outside the range")
            else -> print("none of the above")
        }*/

//        另一种用法是可以检查一个值is或者!is某种特定类型.
//        注意,由于smart casts, 你可以访问该类型的方法和属性而不用额外的检查。

        /*val hasPrefix = when(x) {
            is String -> x.startsWith("prefix")
            else -> false
        }*/

        //when 也可以用来替代if-else if链.
        // 如果不提供参数，所有的分支条件都是简单的布尔值，而当一个分支的条件返回true时，则调用该分支：

        /*when {
            x.isOdd() -> print("x is odd")
            x.isEven() -> print("x is even")
            else -> print("x is funny")
        }*/
    }

    /**
     * 使用范围表达式
     * 查看一个数字是否在某个范围里使用 in 操作符
     */
    fun testIn(x: Int){
//        查看一个数字是否在某个范围里使用 in 操作符:
        val y: Int = 5;
        if (x in 1..y-1) {
            print("OK")
        }
//        检查一个数字是否不在某范围:

//        if (x !in 0..array.lastIndex) {
//            print("Out")
//        }
//        在一个范围内迭代:

        for (x in 1..5) {
            print(x)
        }
    }

    /**
     * 使用集合
     */
    fun testCollections(names: Array<String>){
//        在一个集合里迭代:
        for (name in names) {
            println(name);
        }

//        查看一个集合里是否有某个对象使用 in 操作符:
        if ("lujun" in names){ // names.contains(text) is called
            print("Yes");
        }

//        使用函数来过滤和控制集合:
        names filter { it.startsWith("A") } sortBy { it } map { it.toUpperCase() } forEach { print(it) }
    }


}
