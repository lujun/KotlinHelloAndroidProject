package co.lujun.kotlinhelloandroidproject;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by lujun on 2015/10/15.
 */
public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }
}
